#include "main.h"



int client(struct socket_data socket_1)
{
    int sockfd, portno, n;
    uint16_t size;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[500];

    uint8_t pos[16] = {181,98,1,2,0,0,3,10,0,0,0,0,0,0,0,0};
    uint8_t alm[16] = {181,98,0x0B,0x30,0,0,0x3B,0xBC,0,0,0,0,0,0,0,0};
    uint8_t rxm[16] = {181,98,2,0x15,0,0,0x17,0x47,0,0,0,0,0,0,0,0};
    uint8_t nav[16] = {181,98,1,1,0,0,2,7,0,0,0,0,0,0,0,0};
    uint8_t clk[16] = {181,98,1,0x22,0,0,0x23,0x6A,0,0,0,0,0,0,0,0};


    char conf[9] = {186,1,0,0,0,5,24,25};
	
	char adrf_id = 2;
	char tb338 = 15;

    while(1){
    portno = socket_1.portnum;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    while (sockfd < 0) {
        //error("ERROR opening socket");
    	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	}
    server = gethostbyname(socket_1.ip);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);

    	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        	error("ERROR connecting");

        printf("please press a key");
        fgets(buffer,255,stdin);

        printf("commad: c = conf, u = ubx ");
        bzero(buffer,500);
        fgets(buffer,255,stdin);

	switch(buffer[0])
	{
	case 'c':
        	//printf("ADRFx: ");
        	//fgets((char*)adrf_id,1,stdin);
        	//printf("TB338 x db: ");
        	//fgets(tb338,1,stdin);
		conf[3] = adrf_id;
		conf[4] = 44;
		conf[5] = 1;
		n = write(sockfd,conf,8);
		size = 7;	
	break;

	case 'p':
		n = write(sockfd,pos,16);
		size = 500;
	break;

	case 'a':
		n = write(sockfd,alm,16);
		size = 500;
	break;

	case 'r':
		n = write(sockfd,rxm,16);
		size = 500;
	break;

	case 'o':
		n = write(sockfd,nav,16);
		size = 500;
	break;
	}
        if (n < 0)
             error("ERROR writing to socket");
        bzero(buffer,500);
        n = read(sockfd,buffer,size);

        if (n < 0)
             error("ERROR reading from socket");

        printf("read: ");
	for(int i=0; i<=100; i++){
        	printf("%d - ",buffer[i]);
        	//check_command(buffer,&command);
	}
        printf("\n\r");

    printf("closing socket\n");
    close(sockfd);
    }

    return 0;
}
