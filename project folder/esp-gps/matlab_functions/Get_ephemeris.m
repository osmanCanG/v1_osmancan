function [eph] = Get_ephemeris(Sat_num,s,source,ip)
%--------------------------------------------------------------------------
% This function takes the binary ephemerisis of one SV and creates the
% relevant datas
%--------------------------------------------------------------------------
if(source == 1)

%--------------------------------------------------------------------------
% Poll the ephemeris data
%--------------------------------------------------------------------------
begin_bits = [181;98];
class_ID_bits = [11;49];

fread(s,2);                                                                   % take the last datas and skip them
Buffer = [181;98;11;49;01;00;Sat_num;0;0];


[Buffer(8) , Buffer(9)] = Check_Sum(Buffer);




  fwrite(s,Buffer);                                                           % Emp
% % %emp = fread(s);
 %fread(s)
% 
%--------------------------------------------------------------------------
% Check the data is OK!!!
%--------------------------------------------------------------------------
eph = 0; 

begin = fread(s,2,'uint8'); 
class_ID = fread(s,2,'uint8');

if (size(begin) ~= 2)
    fprintf('Data Error1...\r\n');
    return

elseif ((begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error2...\r\n');
    return
    
else
    fprintf('Data OK...\r\n');
end

Payload = fread(s,1,'uint16');

if(Payload == 0)
    fprintf('No Data...\r\n');
    return
end



%--------------------------------------------------------------------------
% Sat ID & Sat HOW
%--------------------------------------------------------------------------

Sat_ID = fread(s,1,'uint32'); 
%a= Sat_ID

if Sat_ID ~= Sat_num
    
    fprintf('Sat number Error...\r\n');
    return
end



HOW = fread(s,1,'uint32');

%b = dec2hex(HOW)

if HOW == 0
    fprintf('Invalid Ephemeris...\r\n');
    return
end
%--------------------------------------------------------------------------
% Sub Frame 1 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word0 = fread(s,1,'uint32');
% ss = sub_frame1_word0
sub_frame1_word0 = dec2bin(sub_frame1_word0, 32);

% abb = bin2dec(sub_frame1_word0(9 : 18));

week_number = bin2dec(sub_frame1_word0 (9 : 18));
code_on_l2 = bin2dec(sub_frame1_word0 (19 : 20));
sv_accuracy = bin2dec(sub_frame1_word0 (21 : 24));
sv_health = bin2dec(sub_frame1_word0 (25:30));
iodc_2bits = sub_frame1_word0 (31:32);

fread(s,1,'uint32');            % skip Sub Frame 1 Word 1 
fread(s,1,'uint32');            % skip Sub Frame 1 Word 2
fread(s,1,'uint32');            % skip Sub Frame 1 Word 3

%--------------------------------------------------------------------------
% Sub Frame 1 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word4 = fread(s,1,'uint32');
% ss1 = sub_frame1_word4
sub_frame1_word4 = dec2bin(sub_frame1_word4,32);

Tgd =  (mvl2dec(sub_frame1_word4(25:32),1))*2^-31;
%--------------------------------------------------------------------------
% Sub Frame 1 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word5 = fread(s,1,'uint32');
sub_frame1_word5 = dec2bin(sub_frame1_word5,32);

iodc_8bits = sub_frame1_word5(9:16);
Toc = (bin2dec(sub_frame1_word5(17:32)))*2^4;

IODC = bin2dec([iodc_2bits iodc_8bits]);

%--------------------------------------------------------------------------
% Sub Frame 1 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word6 = fread(s,1,'uint32');
sub_frame1_word6 = dec2bin(sub_frame1_word6,32);

Af2 = (mvl2dec(sub_frame1_word6(9:16),1))*2^-55;
Af1 = (mvl2dec(sub_frame1_word6(17:32),1))*2^-43;

%--------------------------------------------------------------------------
% Sub Frame 1 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word7 = fread(s,1,'uint32');
sub_frame1_word7 = dec2bin(sub_frame1_word7,32);

Af0 = (mvl2dec(sub_frame1_word7(9:30),1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word0 = fread(s,1,'uint32');
sub_frame2_word0 = dec2bin(sub_frame2_word0,32);

IODE = bin2dec(sub_frame2_word0(9:16));
Crs =  (mvl2dec(sub_frame2_word0(17:32),1))*2^-5;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 1 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word1 = fread(s,1,'uint32');
sub_frame2_word1 = dec2bin(sub_frame2_word1,32);

deltan = (mvl2dec(sub_frame2_word1(9:24),1))*2^-43;
M0_8bits = sub_frame2_word1(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 2 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word2 = fread(s,1,'uint32');
sub_frame2_word2 = dec2bin(sub_frame2_word2,32);

M0 = (mvl2dec([M0_8bits sub_frame2_word2(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 3 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word3 = fread(s,1,'uint32');
sub_frame2_word3 = dec2bin(sub_frame2_word3,32);

Cuc = (mvl2dec(sub_frame2_word3(9:24),1))*2^-29;
e_8bits = sub_frame2_word3(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word4 = fread(s,1,'uint32');
sub_frame2_word4 = dec2bin(sub_frame2_word4,32);

e =(bin2dec([e_8bits sub_frame2_word4(9:32)]))*2^-33;%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Sub Frame 2 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word5 = fread(s,1,'uint32'); 
sub_frame2_word5 = dec2bin(sub_frame2_word5,32);

Cus =  (mvl2dec(sub_frame2_word5(9:24),1))*2^-29;
roota_8bits = sub_frame2_word5(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word6 = fread(s,1,'uint32');
sub_frame2_word6 = dec2bin(sub_frame2_word6,32);

roota = (bin2dec([roota_8bits sub_frame2_word6(9:32)]))*2^-19;%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Sub Frame 2 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word7 = fread(s,1,'uint32');
sub_frame2_word7 = dec2bin(sub_frame2_word7,32);

Toe= (bin2dec(sub_frame2_word7(9:24)))*2^4;
             % skip fit interval flag and AODO (dont know what are they)

%--------------------------------------------------------------------------
% Sub Frame 3 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word0 = fread(s,1,'uint32');
sub_frame3_word0 = dec2bin(sub_frame3_word0,32);


Cic = (mvl2dec(sub_frame3_word0(9:24),1))*2^-29;
omega0_8bits = sub_frame3_word0(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 1 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word1 = fread(s,1,'uint32');
sub_frame3_word1 = dec2bin(sub_frame3_word1,32);

omega0 = (mvl2dec([omega0_8bits sub_frame3_word1(9:32)],1))*2^-31;


%--------------------------------------------------------------------------
% Sub Frame 3 Word 2 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word2 = fread(s,1,'uint32');
sub_frame3_word2 = dec2bin(sub_frame3_word2,32);

Cis = (mvl2dec(sub_frame3_word2(9:24),1))*2^-29;
i0_8bits = sub_frame3_word2(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 3 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word3 = fread(s,1,'uint32');
sub_frame3_word3 = dec2bin(sub_frame3_word3,32);

i0 = (mvl2dec([i0_8bits sub_frame3_word3(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word4 = fread(s,1,'uint32');
sub_frame3_word4 = dec2bin(sub_frame3_word4,32);

Crc = (mvl2dec(sub_frame3_word4(9:24),1))*2^-5;
omega_8bits = sub_frame3_word4(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word5 = fread(s,1,'uint32');
sub_frame3_word5 = dec2bin(sub_frame3_word5,32);

omega = (mvl2dec([omega_8bits sub_frame3_word5(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word6 = fread(s,1,'uint32');
sub_frame3_word6 = dec2bin(sub_frame3_word6,32);

omegadot = (mvl2dec(sub_frame3_word6(9:32),1))*2^-43;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word7 = fread(s,1,'uint32');
sub_frame3_word7 = dec2bin(sub_frame3_word7,32);

IDOE = bin2dec(sub_frame3_word7(9:16));
IDOT = (mvl2dec(sub_frame3_word7(17:30),1))*2^-43;



eph = [1; Sat_ID; HOW; week_number; code_on_l2; sv_accuracy; sv_health; Tgd; IODC; Toc;
    Af2; Af1; Af0; IODE; Crs; deltan; M0; Cuc; e; Cus;
    roota;Toe; Cic; omega0; Cis; i0; Crc; omega; omegadot; IDOE; IDOT];


else

    t=tcpip(ip, 23,'NetworkRole','Client');
    set(t, 'InputBufferSize', 1024);

%--------------------------------------------------------------------------
% Poll the ephemeris data
%--------------------------------------------------------------------------
begin_bits = [181;98];
class_ID_bits = [11;49];

Buffer = [181;98;11;49;01;00;Sat_num;0;0];


[Buffer(8) , Buffer(9)] = Check_Sum(Buffer);

fclose(t);
fopen(t);

fwrite(t,Buffer);               %ask for the Nav data

% % %emp = fread(s);
 %fread(t)
% 
%--------------------------------------------------------------------------
% Check the data is OK!!!
%--------------------------------------------------------------------------
eph = 0; 
begin = fread(t,2,'uint8');
class_ID = fread(t,2,'uint8');

if (size(begin) ~= 2)
    fprintf('Data Error1...\r\n');
    fclose(t);
    delete(t);
    clear t
    pause(2);
    return

elseif ((begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))

    fprintf('Data Error2...\r\n');
    fclose(t);
    delete(t);
    clear t
    pause(2);
    return
    
else
    fprintf('Data OK...\r\n');
end

%2-byte little endian length of ubx message payload.
%sf:159,172
%https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_%28UBX-13003221%29.pdf
Payload = (fread(t,1,'uint8') + bitshift(fread(t,1,'uint8'),8));

if(Payload == 0)
    fprintf('No Data...\r\n');
    fclose(t);
    delete(t);
    clear t
    pause(2);
    return
end



%--------------------------------------------------------------------------
% Sat ID & Sat HOW
%--------------------------------------------------------------------------

Sat_ID = (fread(t,1,'uint8') + bitshift(fread(t,1,'uint8'),8) + bitshift(fread(t,1,'uint8'),16) + bitshift(fread(t,1,'uint8'),24));
%a= Sat_ID

if Sat_ID ~= Sat_num
    
    fprintf('Sat number Error...\r\n');
    fclose(t);
    delete(t);
    clear t
    pause(2);
    return
end



HOW = fread(t,1,'uint32');


%b = dec2hex(HOW)

if HOW == 0
    fprintf('Invalid Ephemeris...\r\n');
    fclose(t);
delete(t);
clear t
else
%--------------------------------------------------------------------------
% Sub Frame 1 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word0 = fread(t,1,'uint32');
% ss = sub_frame1_word0
sub_frame1_word0 = dec2bin(sub_frame1_word0, 32);

% abb = bin2dec(sub_frame1_word0(9 : 18));

week_number = bin2dec(sub_frame1_word0 (9 : 18));
code_on_l2 = bin2dec(sub_frame1_word0 (19 : 20));
sv_accuracy = bin2dec(sub_frame1_word0 (21 : 24));
sv_health = bin2dec(sub_frame1_word0 (25:30));
iodc_2bits = sub_frame1_word0 (31:32);

fread(t,1,'uint32');            % skip Sub Frame 1 Word 1 
fread(t,1,'uint32');            % skip Sub Frame 1 Word 2
fread(t,1,'uint32');            % skip Sub Frame 1 Word 3

%--------------------------------------------------------------------------
% Sub Frame 1 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word4 = fread(t,1,'uint32');
% ss1 = sub_frame1_word4
sub_frame1_word4 = dec2bin(sub_frame1_word4,32);

Tgd =  (mvl2dec(sub_frame1_word4(25:32),1))*2^-31;
%--------------------------------------------------------------------------
% Sub Frame 1 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word5 = fread(t,1,'uint32');
sub_frame1_word5 = dec2bin(sub_frame1_word5,32);

iodc_8bits = sub_frame1_word5(9:16);
Toc = (bin2dec(sub_frame1_word5(17:32)))*2^4;

IODC = bin2dec([iodc_2bits iodc_8bits]);

%--------------------------------------------------------------------------
% Sub Frame 1 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word6 = fread(t,1,'uint32');
sub_frame1_word6 = dec2bin(sub_frame1_word6,32);

Af2 = (mvl2dec(sub_frame1_word6(9:16),1))*2^-55;
Af1 = (mvl2dec(sub_frame1_word6(17:32),1))*2^-43;

%--------------------------------------------------------------------------
% Sub Frame 1 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame1_word7 = fread(t,1,'uint32');
sub_frame1_word7 = dec2bin(sub_frame1_word7,32);

Af0 = (mvl2dec(sub_frame1_word7(9:30),1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word0 = fread(t,1,'uint32');
sub_frame2_word0 = dec2bin(sub_frame2_word0,32);

IODE = bin2dec(sub_frame2_word0(9:16));
Crs =  (mvl2dec(sub_frame2_word0(17:32),1))*2^-5;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 1 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word1 = fread(t,1,'uint32');
sub_frame2_word1 = dec2bin(sub_frame2_word1,32);

deltan = (mvl2dec(sub_frame2_word1(9:24),1))*2^-43;
M0_8bits = sub_frame2_word1(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 2 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word2 = fread(t,1,'uint32');
sub_frame2_word2 = dec2bin(sub_frame2_word2,32);

M0 = (mvl2dec([M0_8bits sub_frame2_word2(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 2 Word 3 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word3 = fread(t,1,'uint32');
sub_frame2_word3 = dec2bin(sub_frame2_word3,32);

Cuc = (mvl2dec(sub_frame2_word3(9:24),1))*2^-29;
e_8bits = sub_frame2_word3(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word4 = fread(t,1,'uint32');
sub_frame2_word4 = dec2bin(sub_frame2_word4,32);

e =(bin2dec([e_8bits sub_frame2_word4(9:32)]))*2^-33;%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Sub Frame 2 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word5 = fread(t,1,'uint32'); 
sub_frame2_word5 = dec2bin(sub_frame2_word5,32);

Cus =  (mvl2dec(sub_frame2_word5(9:24),1))*2^-29;
roota_8bits = sub_frame2_word5(25:32);

%--------------------------------------------------------------------------
% Sub Frame 2 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word6 = fread(t,1,'uint32');
sub_frame2_word6 = dec2bin(sub_frame2_word6,32);

roota = (bin2dec([roota_8bits sub_frame2_word6(9:32)]))*2^-19;%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% Sub Frame 2 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame2_word7 = fread(t,1,'uint32');
sub_frame2_word7 = dec2bin(sub_frame2_word7,32);

Toe= (bin2dec(sub_frame2_word7(9:24)))*2^4;
             % skip fit interval flag and AODO (dont know what are they)

%--------------------------------------------------------------------------
% Sub Frame 3 Word 0 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word0 = fread(t,1,'uint32');
sub_frame3_word0 = dec2bin(sub_frame3_word0,32);


Cic = (mvl2dec(sub_frame3_word0(9:24),1))*2^-29;
omega0_8bits = sub_frame3_word0(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 1 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word1 = fread(t,1,'uint32');
sub_frame3_word1 = dec2bin(sub_frame3_word1,32);

omega0 = (mvl2dec([omega0_8bits sub_frame3_word1(9:32)],1))*2^-31;


%--------------------------------------------------------------------------
% Sub Frame 3 Word 2 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word2 = fread(t,1,'uint32');
sub_frame3_word2 = dec2bin(sub_frame3_word2,32);

Cis = (mvl2dec(sub_frame3_word2(9:24),1))*2^-29;
i0_8bits = sub_frame3_word2(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 3 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word3 = fread(t,1,'uint32');
sub_frame3_word3 = dec2bin(sub_frame3_word3,32);

i0 = (mvl2dec([i0_8bits sub_frame3_word3(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 4 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word4 = fread(t,1,'uint32');
sub_frame3_word4 = dec2bin(sub_frame3_word4,32);

Crc = (mvl2dec(sub_frame3_word4(9:24),1))*2^-5;
omega_8bits = sub_frame3_word4(25:32);

%--------------------------------------------------------------------------
% Sub Frame 3 Word 5 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word5 = fread(t,1,'uint32');
sub_frame3_word5 = dec2bin(sub_frame3_word5,32);

omega = (mvl2dec([omega_8bits sub_frame3_word5(9:32)],1))*2^-31;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 6 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word6 = fread(t,1,'uint32');
sub_frame3_word6 = dec2bin(sub_frame3_word6,32);

omegadot = (mvl2dec(sub_frame3_word6(9:32),1))*2^-43;

%--------------------------------------------------------------------------
% Sub Frame 3 Word 7 and Relevant Datas
%--------------------------------------------------------------------------
sub_frame3_word7 = fread(t,1,'uint32');
sub_frame3_word7 = dec2bin(sub_frame3_word7,32);

IDOE = bin2dec(sub_frame3_word7(9:16));
IDOT = (mvl2dec(sub_frame3_word7(17:30),1))*2^-43;



eph = [1; Sat_ID; HOW; week_number; code_on_l2; sv_accuracy; sv_health; Tgd; IODC; Toc;
    Af2; Af1; Af0; IODE; Crs; deltan; M0; Cuc; e; Cus;
    roota;Toe; Cic; omega0; Cis; i0; Crc; omega; omegadot; IDOE; IDOT];

fclose(t);
delete(t);
clear t
end
pause(1);

end