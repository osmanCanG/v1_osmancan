function [clk, ITOW_clk, clk_b, clk_d, TAcc, FAcc] = Get_clk_soln(s,source,ip)
%--------------------------------------------------------------------------
%This Fuction asks for NAV-CLK-SOLN, gets the data and specify the relevant
%--------------------------------------------------------------------------
if(source == 1)

begin_bits = [181;98];
class_ID_bits = [01;34];
clk = 0;
ITOW_clk = 0; 
clk_b = 0;
clk_d = 0;
TAcc = 0;
FAcc = 0;

fread(s,2);                                                                   % take the last datas and skip them

fwrite(s,hex2dec(['B5';'62';'01';'22';'00';'00';'23';'6A']));            %ask for the clk data

%--------------------------------------------------------------------------
% Control the Data, Class, find the length of the PAYLOAD
%--------------------------------------------------------------------------
begin = fread(s,2,'uint8'); 
class_ID = fread(s,2,'uint8');
% take the first bits

if (size(begin) ~= 2)
    fprintf('Data ErrorX...\r\n');
    return

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error...\r\n');
    return
    
else
    fprintf('Data OK...\r\n');
end

Payload = fread(s,1,'uint16');

if(Payload == 0)
    fprintf('No Data...\r\n');
    return
end

%--------------------------------------------------------------------------
% Relevant Data
%--------------------------------------------------------------------------

ITOW_clk = fread(s,1,'uint32')*10^-3;
clk_b = fread(s,1,'int32')*10^-9;
clk_d = fread(s,1,'int32')*10^-9;
TAcc = fread(s,1,'uint32')*10^-9;
FAcc = fread(s,1,'uint32')*10^-12;
clk = 1;

else

    t=tcpip(ip, 23,'NetworkRole','Client');
    set(t, 'InputBufferSize', 1024);


begin_bits = [181;98];
class_ID_bits = [01;34];
clk = 0;
ITOW_clk = 0; 
clk_b = 0;
clk_d = 0;
TAcc = 0;
FAcc = 0;

fclose(t);
fopen(t);

fwrite(t,hex2dec(['B5';'62';'01';'22';'00';'00';'23';'6A']));            %ask for the clk data

%--------------------------------------------------------------------------
% Control the Data, Class, find the length of the PAYLOAD
%--------------------------------------------------------------------------
begin = fread(t,2,'uint8'); 
class_ID = fread(t,2,'uint8');
% take the first bits

if (size(begin) ~= 2)
    fprintf('Data ErrorX...\r\n');
    fclose(t);
    delete(t);
    clear t
    return

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error...\r\n');
    fclose(t);
    delete(t);
    clear t
    return
    
else
    fprintf('Data OK...\r\n');
end

%2-byte little endian length of ubx message payload.
%sf:159,172
%https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_%28UBX-13003221%29.pdf
Payload = (fread(t,1,'uint8') + bitshift(fread(t,1,'uint8'),8));

if(Payload == 0)
    fprintf('No Data...\r\n');
    fclose(t);
    delete(t);
    clear t
    return
end

%--------------------------------------------------------------------------
% Relevant Data
%--------------------------------------------------------------------------

ITOW_clk = fread(t,1,'uint32')*10^-3;
clk_b = fread(t,1,'int32')*10^-9;
clk_d = fread(t,1,'int32')*10^-9;
TAcc = fread(t,1,'uint32')*10^-9;
FAcc = fread(t,1,'uint32')*10^-12;
clk = 1;

fclose(t);
delete(t);
clear t
pause(2);

end