function Set_config(rf_cs, att, ip)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp ('Receiver started');
    t=tcpip(ip, 23,'NetworkRole','Client');
    set(t, 'InputBufferSize', 1024);

%while 1
% Wait for connection

disp('Waiting for connection');
fclose(t);
fopen(t);
disp('Connection OK');

% Read data from the socket
 DataReceived1 = 0;

    att = att .* 10;
    att_a = att ./ 256;
    att_a_int = uint8(att_a);
    att_b = mod(att,256);

    if(att_b == att)
       att_over=0;
    else
       att_over=1;
    end
    
    att_over=uint8(att_over);

    sendData = [186,1,0,rf_cs,att_b,att_over,24,25] %0:none 1: 6820 2:6720 sendData = [186,1,0,ADRF_6X20,att_b,1,24,25]

   disp("packet sending")
   fwrite(t, sendData);
   disp("packet sended")
   
   pause(0.01);
   timeout = 0;
   disp("packet waiting..")
   
   while(get(t, 'BytesAvailable') == 0)  % block the program until bytesavailable, we get answer from stm32 here
     pause(0.01);
     timeout = timeout + 1;
     if(timeout >= 1000)
        break
     end
   end
   
   disp("packet receiving")
   DataReceived = fread(t,8,'uint8')
   disp("packet received")

   id  = num2str([DataReceived(3),DataReceived(4),DataReceived(5),DataReceived(6)]) %The received data from stm32 includes ID number of the card (1,2,3, or 4)
 
  fclose(t);
  disp('Connection close');
pause(2);
