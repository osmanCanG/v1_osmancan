function [CK_A , CK_B] = Check_Sum(Buffer)

CK_A = 0;
CK_B = 0;

for i=3:(size(Buffer)-2)
    CK_A = CK_A + Buffer(i);
    if(CK_A > 255)
        CK_A = CK_A - 256;
    end
    
    CK_B = CK_B + CK_A;
    if(CK_B > 255)
        CK_B = CK_B - 256;
    end
end

