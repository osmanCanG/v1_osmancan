function [raw, SV, ITOW, Week, PR_Mes, CP_Mes, DO_Mes, CNO] = Get_raw_data(s,source,ip) % Mes_IQ has been deleted
%--------------------------------------------------------------------------
% This Fuction asks for RXM-RAW, gets the data and specify the relevant data that are belong to RXM-RAW which are:

% ITOW   : Integer millisecond GPS time of week  (ms) --------> rcvTow (R8, Double Precision --> 8 bytes = 64 bits ,  Unit = s)

% Week   : Week number (weeks) --------> week (U2, Unsigned short --> 2 bytes = 16 bits , Unit = weeks)  


% NSV    : # of satellites following --------> numMeas [Number of measurements to follow] (U1, Unsigned char --> 1 bytes = 8 bits ,  Unit = -)


% CP_Mes : Carrier phase measurement [L1 cycles] (cyles) --------> cpMes (R8, Double Precision --> 8 bytes = 64 bits ,  Unit = cycles)    


% PR_Mes : Pseudorange measurement [m] --------> prMes (R8, Double Precision --> 8 bytes = 64 bits ,  Unit = m) 


% DO_Mes : Doppler measurement [Hz] --------> doMes (R4, Single Precision --> 4 bytes = 32 bits ,  Unit = Hz)  


% SV     : Space Vehicle Number --------> svId (U1, Unsigned Char --> 1 bytes = 8 bits ,  Unit = -) 


% MesQI  : Nav Measurements Quality Indicator: (>=4 : PR+DO OK) (>=5 : PR+DO+CP OK) (<6 : likely loss of carrier lock in previous interval) ---- > Does not exist in RAWX Message

 
% CNO    : Signal strength C/No. (dbHz) --------> cno (U1, Unsigned Char --> 1 bytes = 8 bits ,  Unit = dBHz)  


% LLI    : Loss of lock indicator (RINEX definition)   --------> Does not exist in RAWX Message

%---------------------------------------------------------------------------

%     Header    -   Class  -   ID     LengthOfPayload -------  Total Number of bytes at the beginging of the 
%   0xB5  0x62  -   0x02   -  0x15      2 Bytes       -------  message is 6 bytes.

%----------------------PAYLOAD Information----------------------------------
 
%Unit:                     s            weeks          s         -             -              -                            m              cycles               Hz              -             -               -               -                ms             dBHz             m          cycles          Hz            -               -
%Name:                   rcvTow         week         leaps     numMeas      recStat       reserved1       ------         prMes             cpMes              doMes          gnssID         svId          reserved2        freqId          locktime           cno          prStdev       cpStdev       doStdev       trkStat        reserved3                    
%ByteOffset:          0 -------- 7 , 8 -------- 9  ,  10   ,     11      ,    12      , 13 -------- 15 ,  ------    16 -------- 23  ,  24 -------- 31   , 32 -------- 35   ,   36       ,    37       ,      38       ,      39      ,  40 -------- 41   ,    42       ,     43       ,    44       ,    45      ,     46        ,     47
%NumberFormat:             R8             U2          I1         U1           X1             U1[3]        ------          R8                 R8                 R4             U1            U1              U1              U1               U2              U1             X1            X1            X1            X1              U1
%NumberBits:             64 bits       16 bits      8 bits     8 bits       8 bits          24 bits       ------        64 bits            64 bits            32 bits         8 bits        8 bits          8 bits          8 bits          16 bits          8 bits         8 bits        8 bits        8 bits        8 bits          8 bits
%Type:                 D.Precision    Unsi.short    Si.Char   Unsi.Char     Bitfiled       Unsi.Char      ------       D.Precision        D.Precision        S.Precision     Unsi.Char     Unsi.Char       Unsi.Char       Unsi.Char       Unsi.Short       Unsi.Char      Bitfiled      Bitfiled      Bitfiled      Bitfiled        Unsi.Char



%--------------------------------------------------------------------------

if(source == 1)

                        % HEX --> DEC 
begin_bits = [181;98];  % Header Information, B5 --> 181 , 62 --> 98

class_ID_bits = [2;21]; % Class and ID bits,  02 --> 2 , 15 --> 21 


raw = 0;
SV = 0;
ITOW = 0;
Week = 0;
PR_Mes = 0;
CP_Mes = 0;
DO_Mes = 0;
CNO = 0;

fread(s,2);                                                                   % take the last datas and skip them ??




%												Class ID of UBX-RXM-RAWX message
fwrite(s,hex2dec(['B5';'62';'02';'15';'00';'00';'17';'47']));% ask for the raw data and clk % when we send this message for Ublox kit only payload data is sent



%--------------------------------------------------------------------------
% Control the Data, Class, find the length of the PAYLOAD
%--------------------------------------------------------------------------
begin = fread(s,2,'uint8');    %reading the header of UBX-RXM-RAWX message (0xB5 0x62)
class_ID = fread(s,2,'uint8'); %reading the class and ID of UBX-RXM-RAWX message (0x02 0x15)
% take the first bits

if (size(begin) ~= 2)
    fprintf('Data Error 1...\r\n');
    return;

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error 2...\r\n');
    
    return;
    
else
    fprintf('Data OK...\r\n');
end

Payload = fread(s,1,'uint16');

if(Payload == 0)
    fprintf('No Data...\r\n');
    return;
end

%--------------------------------------------------------------------------
% Relevant Data  total size of each payload is 48 bytes
%--------------------------------------------------------------------------

ITOW = fread(s,1,'double');  % Integer millisecond GPS time of week  (s)

Week = fread(s,1,'ushort');         % Week number (weeks)

fread(s,1,'schar');                % skipping leaps data

NSV  = fread(s,1,'uchar');         % # of satellites following

fread(s,4);                        % skipping recStat ( 1 byte) and  reserved1 (3 bytes)

if (NSV == 0)
    fprintf('No Satellites on view...\r\n');
    return;
end

PR_Mes = fread(s,1,'double');
CP_Mes = fread(s,1,'double');
DO_Mes = fread(s,1,'float');

fread(s,1);  % Skipping gnssID byte


SV = fread(s,1,'uchar'); % SV     : Space Vehicle Number

fread(s,4);   %Skipping  reserved2(1 byte), freqId(1 byte), locktime( 2 bytes)

CNO = fread(s,1,'uchar'); % CNO    : Signal strength C/No. (dbHz)

fread(s,5);  %Skipping   prStdev(1 byte), cpStdev(1 byte),  doStdev(1 byte), trkStat(1 byte), reserved3(1 byte)




for i=1:NSV-1

	PR_Mes_temp = fread(s,1,'double');
    CP_Mes_temp = fread(s,1,'double');    
    DO_Mes_temp = fread(s,1,'float');
	
	fread(s,1);  % Skipping gnssID byte
	
    SV_number_temp = fread(s,1,'uint8'); % SV     : Space Vehicle Number
	
	fread(s,4);   %Skipping  reserved2(1 byte), freqId(1 byte), locktime( 2 bytes)
 
    CNO_temp = fread(s,1,'uchar');  % CNO    : Signal strength C/No. (dbHz)
	
	fread(s,5);  %Skipping   prStdev(1 byte), cpStdev(1 byte),  doStdev(1 byte), trkStat(1 byte), reserved3(1 byte)

    
    if (SV_number_temp > 32)
    
    else
	
	PR_Mes = [PR_Mes ; PR_Mes_temp]; %#ok<AGROW>
    CP_Mes = [CP_Mes ; CP_Mes_temp]; %#ok<AGROW>    
    DO_Mes = [DO_Mes ; DO_Mes_temp]; %#ok<AGROW>
    CNO    = [CNO    ; CNO_temp]; %#ok<AGROW>
    SV = [SV ; SV_number_temp]; %#ok<AGROW>
    end
    end
    raw = 1;

else

    t=tcpip(ip, 23,'NetworkRole','Client');
    set(t, 'InputBufferSize', 1024);

                        % HEX --> DEC 
    begin_bits = [181;98];  % Header Information, B5 --> 181 , 62 --> 98

    class_ID_bits = [2;21]; % Class and ID bits,  02 --> 2 , 15 --> 21 


    raw = 0;
    SV = 0;
    ITOW = 0;
    Week = 0;
    PR_Mes = 0;
    CP_Mes = 0;
    DO_Mes = 0;
    CNO = 0;

    fclose(t);
    fopen(t);

    %												Class ID of UBX-RXM-RAWX message
    fwrite(t,hex2dec(['B5';'62';'02';'15';'00';'00';'17';'47']));% ask for the raw data and clk % when we send this message for Ublox kit only payload data is sent



    %--------------------------------------------------------------------------
    % Control the Data, Class, find the length of the PAYLOAD
    %--------------------------------------------------------------------------
    begin = fread(t,2,'uint8');    %reading the header of UBX-RXM-RAWX message (0xB5 0x62)
    class_ID = fread(t,2,'uint8'); %reading the class and ID of UBX-RXM-RAWX message (0x02 0x15)
    % take the first bits

if (size(begin) ~= 2)
    fprintf('Data Error 1...\r\n');
    fclose(t);
    delete(t);
    clear t
    return;

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error 2...\r\n');
    fclose(t);
    delete(t);
    clear t
    return;
    
else
    fprintf('Data OK...\r\n');
end

%2-byte little endian length of ubx message payload.
%sf:159,172
%https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_%28UBX-13003221%29.pdf
Payload = (fread(t,1,'uint8') + bitshift(fread(t,1,'uint8'),8));

if(Payload == 0)
    fprintf('No Data...\r\n');
    fclose(t);
    delete(t);
    clear t
    return;
end

%--------------------------------------------------------------------------
% Relevant Data  total size of each payload is 48 bytes
%--------------------------------------------------------------------------

ITOW = fread(t,1,'double');  % Integer millisecond GPS time of week  (s)

Week = fread(t,1,'ushort');         % Week number (weeks)

fread(t,1,'schar');                % skipping leaps data

NSV  = fread(t,1,'uchar');         % # of satellites following

fread(t,4);                        % skipping recStat ( 1 byte) and  reserved1 (3 bytes)

if (NSV == 0)
    fprintf('No Satellites on view...\r\n');
    fclose(t);
    delete(t);
    clear t
    return;
end

PR_Mes = fread(t,1,'double');
CP_Mes = fread(t,1,'double');
DO_Mes = fread(t,1,'float');

fread(t,1);  % Skipping gnssID byte


SV = fread(t,1,'uchar'); % SV     : Space Vehicle Number

fread(t,4);   %Skipping  reserved2(1 byte), freqId(1 byte), locktime( 2 bytes)

CNO = fread(t,1,'uchar'); % CNO    : Signal strength C/No. (dbHz)

fread(t,5);  %Skipping   prStdev(1 byte), cpStdev(1 byte),  doStdev(1 byte), trkStat(1 byte), reserved3(1 byte)




for i=1:NSV-1

	PR_Mes_temp = fread(t,1,'double');
    CP_Mes_temp = fread(t,1,'double');    
    DO_Mes_temp = fread(t,1,'float');
	
	fread(t,1);  % Skipping gnssID byte
	
    SV_number_temp = fread(t,1,'uint8'); % SV     : Space Vehicle Number
	
	fread(t,4);   %Skipping  reserved2(1 byte), freqId(1 byte), locktime( 2 bytes)
 
    CNO_temp = fread(t,1,'uchar');  % CNO    : Signal strength C/No. (dbHz)
	
	fread(t,5);  %Skipping   prStdev(1 byte), cpStdev(1 byte),  doStdev(1 byte), trkStat(1 byte), reserved3(1 byte)

    
    if (SV_number_temp > 32)
    
    else
	
	PR_Mes = [PR_Mes ; PR_Mes_temp]; %#ok<AGROW>
    CP_Mes = [CP_Mes ; CP_Mes_temp]; %#ok<AGROW>    
    DO_Mes = [DO_Mes ; DO_Mes_temp]; %#ok<AGROW>
    CNO    = [CNO    ; CNO_temp]; %#ok<AGROW>
    SV = [SV ; SV_number_temp]; %#ok<AGROW>
    end
end
raw = 1;
fclose(t);
delete(t);
clear t
pause(2);

end