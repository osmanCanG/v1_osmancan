function [nav, ITOW_Nav, ECEF_X, ECEF_Y, ECEF_Z, PS_Acc] = Get_nav_data(s,source,ip)
%--------------------------------------------------------------------------
%This Fuction asks for NAV-POS-ECEF, gets the data and specify the relevant
%--------------------------------------------------------------------------
if(source == 1)

begin_bits = [181;98];
class_ID_bits = [01;01];
nav = 0;
ITOW_Nav = 0; 
ECEF_X = 0;
ECEF_Y = 0;
ECEF_Z = 0;
PS_Acc = 0;

fread(s,2);                                                                   % take the last datas and skip them

fwrite(s,hex2dec(['B5';'62';'01';'01';'00';'00';'02';'07']));               %ask for the Nav data

%--------------------------------------------------------------------------
% Control the Data, Class, find the length of the PAYLOAD
%--------------------------------------------------------------------------
begin = fread(s,2,'uint8'); 
class_ID = fread(s,2,'uint8');
% take the first bits

if (size(begin) ~= 2)
    fprintf('Data Error...\r\n');
    return

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error...\r\n');
    return
    
else
    fprintf('Data OK...\r\n');
end

Payload = fread(s,1,'uint16');

if(Payload == 0)
    fprintf('No Data...\r\n');
    return
end

%--------------------------------------------------------------------------
% Relevant Data
%--------------------------------------------------------------------------
ITOW_Nav = fread(s,1,'uint32')*10^-3;
ECEF_X = fread(s,1,'int32')*10^-2;
ECEF_Y = fread(s,1,'int32')*10^-2;
ECEF_Z = fread(s,1,'int32')*10^-2;
PS_Acc = fread(s,1,'uint32')*10^-2;
nav =1;

else

    t=tcpip(ip, 23,'NetworkRole','Client');
    set(t, 'InputBufferSize', 1024);

begin_bits = [181;98];
class_ID_bits = [01;01];
nav = 0;
ITOW_Nav = 0; 
ECEF_X = 0;
ECEF_Y = 0;
ECEF_Z = 0;
PS_Acc = 0;

fclose(t);
fopen(t);

fwrite(t,hex2dec(['B5';'62';'01';'01';'00';'00';'02';'07']));               %ask for the Nav data
%--------------------------------------------------------------------------
% Control the Data, Class, find the length of the PAYLOAD
%--------------------------------------------------------------------------
begin = fread(t,2,'uint8');
class_ID = fread(t,2,'uint8');
% take the first bits

if (size(begin) ~= 2)
    fprintf('Data Error...\r\n');
    fclose(t);
    delete(t);
    clear t
    return

elseif ( (begin(1) ~= begin_bits(1)) || (begin(2) ~= begin_bits(2)) || (class_ID(1) ~= class_ID_bits(1)) || (class_ID(2) ~= class_ID_bits(2)))
    fprintf('Data Error...\r\n');
    fclose(t);
    delete(t);
    clear t
    return
    
else
    fprintf('Data OK...\r\n');
end

%2-byte little endian length of ubx message payload.
%sf:159,172
%https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_%28UBX-13003221%29.pdf
Payload = (fread(t,1,'uint8') + bitshift(fread(t,1,'uint8'),8));

if(Payload == 0)
    fprintf('No Data...\r\n');
    fclose(t);
    delete(t);
    clear t
    return
end

%--------------------------------------------------------------------------
% Relevant Data
%--------------------------------------------------------------------------

ITOW_Nav = fread(t,1,'uint32')*10^-3;
ECEF_X = fread(t,1,'int32')*10^-2;
ECEF_Y = fread(t,1,'int32')*10^-2;
ECEF_Z = fread(t,1,'int32')*10^-2;
PS_Acc = fread(t,1,'uint32')*10^-2;
nav =1;

fclose(t);
delete(t);
clear t
pause(2);
end

